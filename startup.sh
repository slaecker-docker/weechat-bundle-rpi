#!/bin/sh

echo ">> Modding user and group"
echo "User GID $GID"
usermod -o -u "$GID" weechat
echo "User Login shell /usr/local/bin/login.sh"
usermod -s "/usr/local/bin/login.sh" weechat
echo "User home directory"
#mkdir -vp /home/weechat
usermod -d "/home/weechat" weechat
echo "User Password"
usermod -p $(openssl passwd $WEECHAT_PW) weechat
usermod -U weechat
echo "Group GID $UID"
groupmod -o -g "$UID" weechat

#rm -fr /weechat
#rm -f /run.sh

echo ">> Preparing Tmux config"
#rm -vf /home/weechat/.tmux.conf
#ln -vfs /data/settings/tmux.conf /home/weechat/.tmux.conf || exit 1
ln -vfs /data/settings/tmux.conf /etc/

echo ">> Preparing sshd config"
#rm -vf /etc/ssh/sshd_config
ln -vfs /data/settings/ssh/sshd_config /etc/ssh/

echo ">> Preparing ssh auth keys"
mkdir -vp /home/weechat/.ssh
#rm -vfr /home/weechat/.ssh/authorized_keys
ln -vfs /data/settings/ssh/authorized_keys /home/weechat/.ssh/
chown -vR weechat:weechat /data/settings/ssh/authorized_keys

echo ">> Preparing weechat config"
rm -vfr /home/weechat/.weechat
ln -vfs /data/settings/weechat /home/weechat/.weechat
chown -vR weechat:weechat /data/settings/weechat

echo ">> Fixing permissions"
mkdir -p /data/downloads
chown -vR weechat:weechat /home/weechat
chown -vR weechat:weechat /data/downloads
chmod 775 /data/downloads
chmod 700 /home/weechat/.ssh
chmod 600 /home/weechat/.ssh/authorized_keys

echo ">> Running weechat tmux session and detach"
su - weechat -c "/usr/bin/tmux -2 new-session -d -s weechat /usr/bin/weechat"
su - weechat -c "/usr/bin/tmux ls"

echo ">> Preparing ssh hostkeys and run sshd"
if ! ls /data/settings/ssh/hostkeys/ssh_host_*_key* 1> /dev/null 2>&1; then
    ssh-keygen -Av
    mv -n /etc/ssh/ssh_host_*_key* /data/settings/ssh/hostkeys/
else
    echo "Using existing keys"
fi
rm -vf /etc/ssh/ssh_host_*_key*
echo "Host keys:"
ls -1 /data/settings/ssh/hostkeys
echo "Linking hostkeys"
ln -vfs /data/settings/ssh/hostkeys/* /etc/ssh/
echo "Starting sshd"
mkdir -p /run/sshd
/usr/sbin/sshd -eD
