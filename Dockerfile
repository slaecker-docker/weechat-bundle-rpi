FROM resin/rpi-raspbian:stretch

ENV UID 9000
ENV GID 9000
ENV WEECHAT_PW weechat

COPY tmux.conf /data/settings/
COPY sshd_config /data/settings/ssh/
COPY authorized_keys /data/settings/ssh/
COPY startup.sh /usr/local/bin/
COPY login.sh /usr/local/bin/

RUN apt-get update && apt-get upgrade -y \
    && for server in hkp://p80.pool.sks-keyservers.net:80 ha.pool.sks-keyservers.net keyserver.ubuntu.com hkp://keyserver.ubuntu.com:80 pgp.mit.edu; do apt-key adv --keyserver "$server" --recv-keys 11E9DE8848F2B65222AA75B8D1820DB22A11534E && break || echo "Trying new server ..."; done \
    && apt-get install -y apt-transport-https locales openssh-server openssl ca-certificates mosh tmux \
    && echo "deb https://weechat.org/raspbian stretch main" > /etc/apt/sources.list.d/weechat.list \
    && sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=en_US.UTF-8 \
    && apt-get update \
    && apt-get install -y weechat-curses weechat-plugins weechat-python weechat-perl \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd -g ${GID} weechat \
    && useradd -u ${UID} -g ${GID} -m -s /bin/bash weechat \
    && mkdir -p /data \
    && chown -R weechat:weechat /data \
    && mkdir -vp /downloads \
    && mkdir -vp /data/settings/weechat \
    && mkdir -vp /data/settings/ssh/hostkeys \
    && chmod +x /usr/local/bin/startup.sh \
    && chmod +x /usr/local/bin/login.sh

EXPOSE 9001 22 60000

WORKDIR /data

ENTRYPOINT ["/bin/sh"]
CMD ["/usr/local/bin/startup.sh"]
